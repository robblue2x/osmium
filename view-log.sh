#!/bin/bash

sudo chown $USER dnsmasq.log
sudo chgrp $USER dnsmasq.log

cat dnsmasq.log \
| grep reply \
| sed -e 's#.*\ reply\ ##g' \
| grep -v CNAME \
| grep -v NODATA \
| sed -e 's#\ is\ .*##g' \
| sort \
| uniq
