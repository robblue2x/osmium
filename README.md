# osmium

Script to configure dnsmasq to block malicious hostnames and domains

## Install

    sudo apt install dnsmasq resolvconf git
    git clone https://gitlab.com/robblue2x/osmium.git
    cd osmium
    sudo ./install

## Uninstall

    cd osmium
    sudo ./uninstall

# FAQ

## What does it do?

-   Copies configuration files to
-   Stops and disables systemd-resolved
-   Starts and enables dnsmasq

## How do I set my own DNS servers?

DNS servers are set in `conf/osmium-conf.conf`

## What's the difference between a 'host' and a 'domain'?

A host is a specific address

-   this is useful when you want to block a particular server
-   e.g. `pubads.g.doubleclick.net`
-   block only `pubads.g.doubleclick.net`

A domain is a collection of hosts

-   this is useful when you want to block an entire collection of servers
-   e.g. `doubleclick.net`
-   block everything ending in `doubleclick.net`, block all the following:
    -   `pubads.g.doubleclick.net`
    -   `googleads.g.doubleclick.net`

## Why not use `/etc/hosts`?

Using the hosts file is very fast and very powerful, however it does not support
 domains or wildcards. i.e. I cannot block everything ending in `doubleclick.net`

You can still use `/etc/hosts` to block hosts.

## Why not use a browser based ad blocker?

Browser ad blockers only block malicious domains within the particular browser,
 dnsmasq blocks all requests from the system it's installed on. Browser based
 blockers are much slower than dnsmasq.

You can still use a browser based ad blocker with osmium.

## Where does the host list come from?

Some very nice people maintain hosts files for blocking malicious servers,
 osmium pulls some of these file and concatenates them into one big list.
 osmium comes with some built in lists under `builtin`.

See `update` script for sources, comment out the lines to disable a source.

## Where does the domains list come from?

Blocked domains come from `builtin/domains` and `custom/domains`, the built in
 list is a list I have created over the years.

## How can I contribute to the builtin lists?

You can create an MR at <https://gitlab.com/robblue2x/osmium>

# Advanced

## Adding your own block lists

-   hosts are added in `custom/hosts` one per line
-   domains are added in `custom/domains` one per line
-   see `builtin` directory for examples
-   run `./update` to rebuild the lists under `dist`

## Updating the hosts list

    sudo apt install wget sed grep dnsmasq resolvconf git
    git clone https://gitlab.com/robblue2x/osmium.git
    cd osmium
    git pull
    ./update
    sudo ./install

## Capturing system DNS requests

A script `capture` is provided. It will:

-   stop dnsmasq service
-   run dnsmasq with no-daemon and logging turned on
-   wait for you to kill dnsmasq with `Ctrl+C`
-   restart dnsmasq service

## Docker guests

It may be desirable to use your machine as a DNS server for docker guests,
 meaning all DNS queries get routed through your dnsmasq server.
This blocks your containers from accissing bad hosts, speeds up DNS requests,
 and allows you to use a hostname to refer to your machine from within docker
 containers.

Open `/etc/hosts` with  sudo

    sudo nano /etc/hosts

Add a host entry for docker host

    172.17.0.1 host.docker.internal

Restart `dnsmasq` after editing `/etc/hosts`

    sudo service dnsmasq restart

Open dockerd config with sudo

    sudo nano /etc/docker/daemon.json

Add the following config (or append if a config exists)

    {
      "dns":["172.17.0.1"]
    }

Restart docker

    sudo service docker restart
